class Code
  # Possible pegs
  PEGS = {
    r: "red",
    g: "green",
    b: "blue",
    y: "yellow",
    o: "orange",
    p: "purple"
  }

  # Code's pegs
  @pegs

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  # Random Code factory
  def self.random
    colors = []
    4.times { colors.push(PEGS.to_a.sample[0]) }
    Code.new(colors)
  end

  # Input based Code factory
  def self.parse(input)
    pegs = input.downcase.chars.map(&:to_sym)
    pegs.each { |sym| raise if !PEGS.include?(sym) }
    Code.new(pegs)
  end

  def [](index)
    @pegs[index]
  end

  # returns number of matches
  def exact_matches(other)
    matches = 0
    @pegs.each_index do |index|
      matches += 1 if self.pegs[index] == other.pegs[index]
    end
    matches
  end

  # self count hash for individual peg colors
  # helper method for #near_matches(other)
  def peg_count
    count = Hash.new(0)
    @pegs.each { |peg| count[peg] += 1 }
    count
  end

  # returns number of color but not position matches
  def near_matches(other)
    # construct other's peg count as we iterate
    match_hash = Hash.new(0)

    matches = 0
    other.pegs.each_with_index do |peg, index|
      # Remove current index to avoid exact match
      removed_index = (@pegs[0...index] + @pegs[index + 1..-1])
      # condition one: near match ; condition two: accounts for peg counts
      if removed_index.include?(peg) && peg_count[index] >= match_hash[peg]
        match_hash[peg] += 1
        matches += 1
      end
    end

    matches - exact_matches(other)
  end



  def ==(other)
    # Validate input
    return false if other.class != Code
    # compare
    exact_matches(other) == 4 ? true : false
  end
end

class Game
  @secret_code

  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Input four character peg sequence"
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    near_matches = @secret_code.near_matches(code)
    exact_matches = @secret_code.exact_matches(code)
    print "near matches: #{near_matches} and exact matches: #{near_matches}"
  end
end
